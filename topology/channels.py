# Channel objects

import re
import graphbase

class ChannelBase(graphbase.ArcBase):
    'Base class for all channels'

    def __init__(self, datatype=None, **kwargs):
        graphbase.ArcBase.__init__(self, **kwargs)
        self.datatype = datatype

    def copy(self):
        new = self.__class__(name=self.name, descr=self.descr,
                             datatype=self.datatype)
        return new

    def isChannel(self):
        return True

    def isContent(self):
        return False

    def isControl(self):
        return False

    def isEvent(self):
        return False

    def isFeedback(self):
        return False

    def __str__(self):
        out = [self.flavor.capitalize(), 'Channel']
        if self.name:
            out.append('"%s"' % (self.name,))
        #if self.guid:
        #    out.append('guid=%r' % (self.guid,))
        if self.description is not None:
            s = re.sub(r'\n', '/', self.description)
            if s > 20:
                s = s[:17] + '...'
            out.append('"%s"' % (s,))
        return ' '.join(out)

    ## def genInGraph(self, hits, other):
    ##     outputs = self.outputs()
    ##     if not outputs:                 # default
    ##         for p in NodeArcBase.genGraph(self, hits):
    ##             yield p
    ##     else:
    ##         a = [self.flavor.capitalize()]
    ##         if self.name:
    ##             out.append('"%s"' % (self.name,))
    ##         label = re.sub(r'"', '\\"', ' '.join(a))
    ##         for item in outputs:
    ##             yield '"%s" -> "%s" [label = "%s"]' % (other.getGuid(),
    ##                                                    item.getGuid(),
    ##                                                    label)
    ##             for p in item.genGraph(hits):
    ##                 yield p


class ChannelContent(ChannelBase):
    flavor = 'content'

    def isContent(self):
        return True


class ChannelControl(ChannelBase):
    flavor = 'control'

    def isControl(self):
        return True

class ChannelEvent(ChannelBase):
    flavor = 'event'

    def isEvent(self):
        return True

class ChannelFeedback(ChannelBase):
    flavor = 'feedback'

    def isFeedback(self):
        return True
