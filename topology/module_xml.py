#!/usr/bin/env python

import sys
import re
import xml.dom
import xml.dom.minidom

import graphbase
import modules
import connectors
import channels

ForceGuids = True
TraceConnections = False
TraceTags = False



class Context(object):
    patDots = re.compile(r'([^\.]*\.?)')
    patDot = re.compile(r'([^\.]*\.)(.+)')

    def __init__(self, parent=None, name=None):
        self.parent = parent
        self.name = name
        self.symbols = {}

    def copy(self, parent=None, name=None):
        if parent is None:
            parent = self.parent
        if name is None:
            name = self.name
        new = self.__class__(parent=parent, name=name)
        new.symbols = self.symbols.copy()
        return new

    def duplicate(self, parent=None, name=None, changed=None):
        if parent is None:
            parent = self.parent
        if name is None:
            name = self.name
        new = self.__class__(parent=parent, name=name)
        if changed is None:
            dup = lambda x: x
        else:
            dup = lambda x: changed.map(x)
        for k, v in self.symbols.items():
            new.symbols[k] = dup(v)
        return new

    def globalContext(self):
        if self.parent is None or self.parent is self:
            return self
        return self.parent.globalContext()

    def scope(self, name=None):
        out = []
        if self.parent is not None and self.parent is not self:
            out = [self.parent.scope(), self.name]
        return '.'.join([x for x in out + [name] if x])

    def get(self, key, dflt=None):
        '''
        Get a value, with an optional dotted namespace,
        going up thru the scopes if necessary.
        '''
        m = self.patDot.match(key)
        if m:
            cntx = self.get(m.group(1))
            if cntx is None:
                item = dflt
            else:
                key = m.group(2)
                item = cntx.get(key, dflt)
        else:
            item = self.symbols.get(key)
            if item is None and self.parent is not None:
                item = self.parent.get(key)
            if item is None:
                item = dflt
        return item

    def __getitem__(self, key):
        item = self.symbols.get(key)
        if item is None and self.parent is not None:
            item = self.parent.get(key)
        if item is None:
            raise KeyError('%r not found' % (key,))
        return item

    def __setitem__(self, key, value):
        self.symbols[key] = value

    def set(self, key, value):
        #trace('  %s:set(%r,%r)' % (self.name, key, value))
        if value is None:
            raise ValueError('Set %r to None!' % (key,))
        self.symbols[key] = value
        ## if propagate and self.parent is not None:
        ##     self.parent.setCond(key, value, propagate)

    def setCond(self, key, value):
        'Set key-value iff key not already set'
        if key not in self.symbols:
            self.set(key, value)

    def setGlobal(self, key, value):
        self.globalContext.set(key, value)

    def __len__(self):
        return len(self.symbols)

    def title(self):
        title = self.scope()
        if not title:
            title = self.name
        return title
 
    def __str__(self):
        out = [self.__class__.__name__, self.title(),
               '%d entries' % (len(self),)]
        return ' '.join([x for x in out if x])

    def Dump(self, depth=1):
        kk = self.symbols.keys()
        if not kk:
            yield 'Context %r EMPTY' % (self.title(),)
        else:
            yield 'Context %r' % (self.title(),)
            maxlen = max([len(x) for x in kk])
            kk.sort()
            for k in kk:
                yield '    %s = %s' % (k.ljust(maxlen), self.symbols[k])
        depth -= 1
        if depth <= 0:
            yield '....'
        elif self.parent is not None:
            for p in self.parent.Dump(depth):
                yield p

########################################################################

class ModuleDefinitionTree(object):
    modFunction = modules.ModFunction

    def __init__(self, file=None, dump=True):
        self.canDump = dump
        self._definitions = None
        self._globalContext = Context(name='GLOBAL')
        if file is not None:
            self.parse(file)

    def definitions(self):
        return self._definitions

    def symbols(self):
        return self._globalContext.symbols

    def parse(self, file):
        tree = xml.dom.minidom.parse(file)
        self.evalNode(tree.documentElement)

    def error(self, msg):
        print 'ERROR: %s' % (msg,)

    def warn(self, msg):
        print 'WARNING: %s' % (msg,)

    def note(self, msg):
        print 'NOTE: %s' % (msg,)

    def Dump(self):
        if self._definitions is None:
            yield 'No definitions.'
        else:
            ord = 0
            for node in self._definitions:
                ord += 1
                t = '%2d:' % (ord,)
                tb = ' ' * len(t)
                for p in node.Dump():
                    yield '%s %s' % (t, p)
                    t = tb
            yield ''
        yield 'Context:'
        for p in self._globalContext.Dump():
            yield '  %s' % (p,)

    def evalNode(self, node, context=None):
        retval = None
        traceNode(node)
        if node.nodeType == node.ELEMENT_NODE:
            op = self.optbl.get(node.tagName)
            if op is None:
                op = self.optbl.get(node.tagName.lower())
            if op is not None:
                if context is None:
                    context = self._globalContext
                retval = op(self, node, context)
            else:
                self.note('UNKNOWN tag: %r' % (node.tagName,))
        traceNodeEnd(node)
        return retval

    def resolve(self, key, context):
        value = context.get(key)
        if value is None:
            value = Placeholder(key, context) # resolve when needed
        return value

    def do_define(self, node, context):
        self._definitions = self.consume_subtree(node, context)

    def do_module(self, node, context):
        t = attr(node, 'type')
        if t is None:
            if attr(node, 'lib'):
                t = 'external'
            elif attr(node, 'key'):
                t = 'internal'
            elif attr(node, 'function'):
                t = 'inline'
            else:
                t = 'internal'
        name = attr(node, 'name')
        modId = attr(node, 'id')
        descr = attr(node, 'descr', 'description', 'help')
        guid = attr(node, 'guid', 'uuid')
        if t == 'internal':
            fnct = attr(node, 'key')
            if fnct is None:
                funct = name
            module = modules.ModuleInternal(name=name,
                                            guid=guid,
                                            modId=modId,
                                            descr=descr,
                                            function=fnct)
        elif t == 'external':
            module = modules.ModuleExternal(name=name,
                                            guid=guid,
                                            modId=modId,
                                            descr=descr,
                                            function=attr(node, 'lib'))
        elif t == 'inline':
            module = modules.ModuleInline(name=name,
                                          guid=guid,
                                          modId=modId,
                                          descr=descr)
        else:
            self.error('Unrecognized module type %r' % (t,))
            return
        return self.consume_module(node, module, context, name)

    def consume_module(self, node, module, context, name):
        if ForceGuids:
            module.getGuid()
        newContext = Context(parent=context, name=name) # push context
        if name:
            context.set(name, module)
            context.set(name + '.', newContext)
        if module.modId:
            context.set(module.modId, module)
        newContext.set(' module', module)
        for kid in order_nodes(node.childNodes):
            item = self.evalNode(kid, newContext)
            if item is None:
                continue
            if isinstance(item, connectors.ConnectorBase):
                module.addConnector(item)
            elif isinstance(item, self.modFunction):
                module.function.add(item)
            else:
                #trace('SKIPPED: %r' % (item,))
                pass
        return module

    def consume_subtree(self, node, context):
        out = []
        for kid in order_nodes(node.childNodes):
            item = self.evalNode(kid, context)
            if item is not None:
                out.append(item)
        return out

    def make_conn(self, node, context, cls):
        name = attr(node, 'name')
        item = cls(name=name,
                   guid=attr(node, 'guid', 'uuid'),
                   conId=attr(node, 'id'),
                   descr=attr(node, 'descr', 'description', 'help'),
                   ref=attr(node, 'ref', 'reference'),
                   value=attr(node, 'value'),
                   datatype=attr(node, 'type'))
        if ForceGuids:
            item.getGuid()
        if name:
            context.set(name, item)
        return item
        
    def do_conn_input(self, node, context, cls):
        return self.make_conn(node, context, cls)

    def do_conn_output(self, node, context, cls):
        return self.make_conn(node, context, cls)

    def do_channel(self, node, context, cls):
        name = attr(node, 'name')
        item = cls(name=name,
                   guid=attr(node, 'guid', 'uuid'),
                   descr=attr(node, 'descr', 'description', 'help'),
                   datatype=attr(node, 'type'))
        if name:
            context.set(name, item)
        if ForceGuids:
            item.getGuid()
        tree = self.consume_subtree(node, context)

        # $$$$ What do we do with the tree?
        
        src = attr(node, 'from')
        dst = attr(node, 'to')
        if src:
            x = self.resolve(src, context)
            if TraceConnections:
                trace('CHAN %s <- %s' % (item, x))
            item.addInput(x)
        if dst:
            x = self.resolve(dst, context)
            if TraceConnections:
                trace('CHAN %s -> %s' % (item, x))
            item.addOutput(x)
        if dst and src:
            return None                 # internal connection
        return item

    def do_import(self, node, context):
        name = attr(node, 'name')
        ref = attr(node, 'ref', 'reference')
        if not ref:
            self.error('<import> name=%r missing "ref="' % (name,))
            return None
        retval = None
        item = context.get(ref)
        if item is None:
            self.error('<import ref=%r> not found' % (ref,))
        elif isinstance(item, modules.ModuleBase):
            itemOld = item
            changed = graphbase.DuplicateNodes()
            item = item.duplicate(changed=changed)
            if name:
                item.name = name
            if ForceGuids:
                item.getGuid()
            #trace('DUPLICATE: %s -> %s' % (id(itemOld), id(item)))
            cntx = context.get(ref + '.')
            if isinstance(cntx, Context):
                context = cntx.duplicate(parent=context,
                                         name=name + '.', changed=changed)
            else:
                trace('(bad context for %r: %r)' % (ref, cntx))
            retval = self.consume_module(node, item, context, name)
        else:
            self.error('<import ref=%r> expected module, got %s' % \
                       (ref, item.__class__.__name__))
        return retval

    def do_function(self, node, context):
        out = None
        for kid in node.childNodes:
            if kid.nodeType == kid.TEXT_NODE:
                text = kid.data
                if out is None:
                    out = self.modFunction(text)
                else:
                    out.add(text)
            else:
                trace('SKIP_FUN: %r' % (kid,))
        return out

    def do_dump(self, node, context):
        if self.canDump:
            name = attr(node, 'ref', 'name')
            if name:
                item = context.get(name)
                if item is None:
                    self.error('No item called %r' % (name,))
                    return None
                direction = attr(node, 'direction', 'dir')
                if direction and direction.lower() == 'in':
                    op = item.DumpIn
                elif direction and direction.lower() == 'out':
                    op = item.DumpOut
                else:
                    op = item.Dump
                trace(' Dump '.center(78, '-'))
                for p in op():
                    trace(p)
                trace(' End Of Dump '.center(78, '-'))
            else:
                trace(' Context Dump '.center(78, '-'))
                for p in context.Dump(100):
                    trace(p)
                trace(' End Of Dump '.center(78, '-'))
        return None

    optbl = {
        'define':
        lambda s, n, e:s.do_define(n, e),

        'definitions':
        lambda s, n, e:s.do_define(n, e),

        'module':
        lambda s, n, e: s.do_module(n, e),

        'content_input':
        lambda s, n, e: s.do_conn_input(n, e,
                                        connectors.ConnInContent),
        'content_output':
        lambda s, n, e: s.do_conn_output(n, e,
                                         connectors.ConnOutContent),
        'content_channel':
        lambda s, n, e: s.do_channel(n, e,
                                     channels.ChannelContent),
        'control_input':
        lambda s, n, e: s.do_conn_input(n, e,
                                        connectors.ConnInControl),
        'control_output':
        lambda s, n, e: s.do_conn_output(n, e,
                                         connectors.ConnOutControl),
        'control_channel':
        lambda s, n, e: s.do_channel(n, e,
                                     channels.ChannelControl),
        'event_input':
        lambda s, n, e: s.do_conn_input(n, e,
                                        connectors.ConnInEvent),
        'event_output':
        lambda s, n, e: s.do_conn_output(n, e,
                                         connectors.ConnOutEvent),
        'event_channel':
        lambda s, n, e: s.do_channel(n, e,
                                     channels.ChannelEvent),
        'feedback_input':
        lambda s, n, e: s.do_conn_input(n, e,
                                        connectors.ConnInFeedback),
        'feedback_output':
        lambda s, n, e: s.do_conn_output(n, e,
                                         connectors.ConnOutFeedback),
        'feedback_channel':
        lambda s, n, e: s.do_channel(n, e,
                                     channels.ChannelFeedback),
        'import':
        lambda s, n, e: s.do_import(n, e),

        'function':
        lambda s, n, e: s.do_function(n, e),

        'dump':
        lambda s, n, e: s.do_dump(n, e),

        }


## class ModuleFunction(object):

##     def __init__(self, text=None):
##         self.text = []
##         if text is not None:
##             self.addText(text)

##     def __len__(self):
##         return len(self.text)

##     def addText(self, t):
##         if t:
##             if not isinstance(t, (list, tuple)):
##                 t = (t,)
##             for s in t:
##                 if not re.match(r'\s*$', s):
##                     self.text.extend(s.splitlines())

##     def __iadd__(self, other):
##         if isinstance(other, ModuleFunction):
##             self.addText(other.text)
##         else:
##             self.addText(str(other))

##     def __add__(self, other):
##         new = self.__class__(self.text)
##         new += other
##         return new

##     def __repr__(self):
##         return '%s.%s(%r)' % (self.__module__,
##                               self.__class__.__name__,
##                               self.text)


class Placeholder(object):
    patQuestion = re.compile(r'is[A-Z]')

    def __init__(self, key, context):
        self.key = key
        self.context = context
        self.ref = None

    def resolve(self):
        if self.ref is None:
            item = self.context.get(self.key)
            if item is not None:
                self.ref = item
                self.key = None         # clean up
                self.context = None     # clean up
        return self.ref is not None

    def __str__(self):
        if self.ref is not None:
            return str(self.ref)
        return '%s(%r,%s)' % (self.__class__.__name__,
                              self.key, self.context)

    def __getattr__(self, name):
        if self.ref is None and not self.resolve():
            if self.patQuestion.match(name):
                return lambda : False  # answer unknown questions "NO!"
            raise AttributeError('Unresolved %r %s' % (name, self))
        return getattr(self.ref, name)

############################################################################

def attr(node, *keys):
    for k in keys:
        x = node.getAttribute(k)
        if x:
            return x
    return None


def order_nodes(nodes):
    return nodes


def channels_to_end(items):
    out = []
    chan = []
    for node in items:
        if node.nodeType == node.ELEMENT_NODE and \
               node.tagName.endswith('channel'):
            chan.append(node)
        else:
            out.append(node)
    return out + chan


def trace(msg):
    print msg


def traceNode(node):
    if TraceTags and node.nodeType == node.ELEMENT_NODE:
        trace('<%s %s>' % (node.tagName, fmt_attr(node)))


def traceNodeEnd(node):
    if TraceTags and node.nodeType == node.ELEMENT_NODE:
        trace('</%s>' % (node.tagName,))


def fmt_attr(node):
    trystr = lambda x: str(x)
    attr = node.attributes
    out = []
    if attr:
        for n in xrange(attr.length):
            a = attr.item(n)
            out.append('%s=%r' % (a.name, trystr(a.value)))
    return ' '.join(out)
    


if __name__ == '__main__':
    path = '-'
    if len(sys.argv) > 1:
        path = sys.argv[1]
    if path == '-':
        infd = sys.stdin
    else:
        infd = open(path)
    mdt = ModuleDefinitionTree(infd)
    for p in mdt.Dump():
        print p

