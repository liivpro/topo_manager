#!/usr/bin/env python

import sys
import json

import modules
import connectors
import channels


def readJson(file=None):
    if file is None:
        file = '-'
    if ininstance(file, str):
        if file == '-':
            return readJson(sys.stdin)
        fd = open(file)
        retval = readJson(fd)
        fd.close()
        return retval
    
            
