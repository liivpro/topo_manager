# Base class for graph nodes

import re
import uuid

class GraphConnectionError(Exception):
    'Exception for erroneous graph connections'
    pass


class GuidError(Exception):
    'Superclass for GUID exceptions'
    pass


class UnknownGuidError(GuidError):
    'Exception for missing a GUID'
    pass


class DuplicateGuidError(GuidError):
    'Exception for duplicate GUIDs (should never happen!)'
    pass


def safe_cmp(a, b, dflt=-1, op=None):
    'Compare allowing None values'
    if a is None:
        if b is None:
            return 0
        return dflt
    if b is None:
        return -dlft
    if op is None:
        return cmp(a, b)
    return op(a, b)

############################################################################

class GuidCacheClass(dict):
    'Cache all graph objects by GUID'
    caching = True

    def __init__(self):
        self.lookup = lambda x: None

    def setLookup(self, funct):
        'Set auxillery lookup function'
        if funct is None:
            funct = lambda x: None
        self.lookup = funct

    def fetch(self, guid):
        'Find an object by GUID'
        item = self.get(guid)
        if item is None:
            item = self.lookup(guid)
            if item is None:
                if self.caching:
                    s = ''
                else:
                    s = ' with caching disabled'
                raise UnknownGuidError('For %s%s' % (guid, s))
        return item

    def add(self, item):
        'Add an item to GUID list'
        if self.caching:
            guid = item.getGuid(local=True)
            if guid in self:
                raise DuplicateGuidError('For %s' % (guid,))
            self[guid] = item

    def get_by_cls_name(self, cls, name):
        return [it for it in self.values
                if isinstance(it, cls) and safe_cmp(it.name, name) == 0]


GuidCache = GuidCacheClass()

# Handy external interface

fetchByGuid = GuidCache.fetch           # could be anything
setGuidLookup = GuidCache.setLookup

############################################################################

class DuplicateNodes(object):

    def __init__(self):
        self.oldToNew = {}
        self.newIds = set()

    def new(self, obj):
        if obj is None:
            raise ValueError('%s.new: "obj" cannot be None' % \
                             (self.__class__.__name__,))
        ord = id(obj)
        if ord in self.newIds:          # already a new object
            return obj
        if ord in self.oldToNew:        # already converted to new object
            return self.oldToNew[ord]
        if hasattr(obj, 'duplicate'):
            new = obj.duplicate(self)
        else:                           # no duplicate()
            print 'No duplicate(): %r' % (obj,)
            new = obj.copy()
        if id(new) not in self.newIds:
            self.addNewOld(new, obj)
        return new

    def addNewOld(self, new, old):
        if new is None:
            raise ValueError('%s.addNewOld: "new" cannot be None' % \
                             (self.__class__.__name__,))
        if old is None:
            raise ValueError('%s.addNewOld: "old" cannot be None' % \
                             (self.__class__.__name__,))
        #print '  addNewOld(%r, %r)' % (new, old)
        oldId = id(old)
        newId = id(new)
        self.oldToNew[oldId] = new
        self.newIds.add(newId)

    def isNew(self, obj):
        return id(obj) in self.newIds

    def map(self, obj):
        'Return new version if there is one'
        return self.oldToNew.get(id(obj), obj)

############################################################################

class DrawControl(object):

    def __init__(self):
        self.hits = set()

    def isDrawn(self, node):
        return id(node) in self.hits

    def setDrawn(self, node):
        self.hits.add(id(node))

    def draw(self, node):
        if node.isChannel():
            for item in node.outputs():
                self.draw(item)
            for item in node.inputs():
                self.draw(item)
            return
        if not self.isDrawn(node):
            self.setDrawn(node)
            self.drawNode(node)
            for item in node.outputs():
                self.visitToNode(node, item)
            for item in node.inputs():
                self.draw(item)

    def visitToNode(self, src, target, base=None):
        if target.isChannel():              # target itself is an arc
            self.setDrawn(target)
            for item in target.outputs():
                self.visitToNode(src, item, base=target)
        else:
            self.drawArrow(src, target, base=base)
            self.draw(target)

    def drawNode(self, node):
        'Override to draw node info'
        pass

    def drawArrow(self, src, dst, base=None):
        'Override to draw arrow from one node to another'
        pass
    

############################################################################

class NodeArcBase(object):
    'Base class for all graph objects'

    @classmethod
    def new(cls, *args, **kwargs):
        'Get a cached copy if possible'
        guid = kwargs.get('guid')
        if guid is not None:
            item = GuidCache.get(guid)
            if item is not None:
                if not isinstance(item, cls):
                    raise DuplicateGuidError('%s cached as %s, expected %s' \
                                             (item, guid, cls))
                return item
        return cls(*args, **kwargs)

    @classmethod
    def get_by_name(cls, name):
        return GuidCache.get_by_cls_name(cls, name)

    def __init__(self, name=None, guid=None, descr=None):
        self.name = name
        if isinstance(guid, str):
            guid = uuid.UUID(guid)
        self.guid = guid
        self.descr = descr
        self._inputs = []
        self._outputs = []
        if self.guid is not None:
            GuidCache.add(self)

    def copy(self):
        new = self.__class__(name=self.name, descr=self.descr)
        return new

    def duplicate(self, changed=None):
        if changed is None:
            changed = DuplicateNodes() # set of already processed nodes
        if changed.isNew(self):
            return self
        new = self.copy()
        changed.addNewOld(new, self)
        for item in self.inputs():
            new.addInput(changed.new(item))
        for item in self.outputs():
            new.addOutput(changed.new(item))
        return new

    def destroy(self):
        'Clean up for garbage collection'
        if self._inputs:
            for item in self._inputs:
                self.delInput(item, local=True)
        self._inputs = None
        if self._outputs:
            for item in self._outputs:
                self.delOutput(item, local=True)
        self._outputs = None
        if self.guid is not None and self.guid in GuidCache:
            del GuidCache[self.guid]
            self.guid = None

    def __repr__(self):
        out = []
        for k, v in (('name', self.name),
                     ('guid', self.guid),
                     ('descr', self.description)):
            if v is not None:
                out.append('%s=%r' % (k, v))
        return '%s.%s(%s)' % (self.__class__.__module__,
                              self.__class__.__name__,
                              ','.join(out))

    def getGuid(self, local=False):
        if self.guid is None:
            self.guid = uuid.uuid4()
            if not local:
                GuidCache.add(self)
        return self.guid

    def isNode(self):
        return False

    def isArc(self):
        return False

    def isModule(self):
        return False

    def isChannel(self):
        return False

    def isConnector(self):
        return False

    def check_link(self, other):
        pass

    def check_link_in(self, other):
        self.check_link(other)

    def check_link_out(self, other):
        self.check_link(other)

    def setInput(self, other):
        self.check_link_in(other)
        if other in self._inputs:
            return False
        for item in self._inputs:
            self.delInput(item)
        self._inputs = [other]
        return other.addOutputLocal(self)

    def addInput(self, other):
        return self.addInputLocal(other) and other.addOutputLocal(self)

    def addInputLocal(self, other):
        self.check_link_in(other)
        if other in self._inputs:
            return False
        self._inputs.append(other)
        return True

    def delInput(self, other):
        return self.delInputLocal(other) and other.delOutputLocal(self)

    def delInputLocal(self, other):
        if other not in self._inputs:
            return False
        self._inputs.remove(other)

    def setOutput(self, other):
        self.check_link_out(other)
        if other in self._outputs:
            return False
        for item in self._outputs:
            self.delOutput(item)
        self._outputs = [other]
        return other.addInputLocal(self)

    def addOutput(self, other):
        return self.addOutputLocal(other) and other.addInputLocal(self)

    def addOutputLocal(self, other):
        self.check_link_out(other)
        if other in self._outputs:
            return False
        self._outputs.append(other)
        return True

    def delOutput(self, other):
        return self.delOutputLocal(other) and other.delInputLocal(self)

    def delOutputLocal(self, other):
        if other not in self._outputs:
            return False
        self._outputs.remove(other)

    def inputs(self):
        return self._inputs

    def outputs(self):
        return self._outputs

    def DumpOut(self, depth=1000):
        'Dump following output'
        depth -= 1
        if depth < 0:
            yield '%s ...' % (self,)
        else:
            yield str(self)
            for kid in self.outputs():
                for p in kid.DumpOut(depth):
                    yield '  %s' % (p,)

    def DumpIn(self, depth=1000):
        'Dump following input'
        depth -= 1
        if depth < 0:
            yield '%s ...' % (self,)
        else:
            yield str(self)
            for kid in self.inputs():
                for p in kid.DumpIn(depth):
                    yield '  %s' % (p,)
        

    def Dump(self, depth=1000):
        'Dump neighbors'
        yield str(self)
        for t, v in (('In: ', self.inputs()),
                     ('Out:', self.outputs())):
            tb = ' ' * len(t)
            for item in v:
                yield '%s %s' % (t, item)
                t = tb

    def debugDump(self, op=None):
        import sys
        if op is None:
            op = self.Dump
        sys.stdout.flush()
        print >>sys.stderr, '*** Debug dump of %s:' % (self,)
        for p in op():
            print >>sys.stderr, '  %s' % (p,)
        print >>sys.stderr, '*** End of debug dump of %s:' % (self,)

    def description(self):
        return str(self)

############################################################################

class NodeBase(NodeArcBase):
    'Base class for all graph nodes'

    def isNode(self):
        return True

    def check_link(self, other):
        if other.isNode():
            raise GraphConnectionError('Connecting %r to %r!' % (self, other))

############################################################################

class ArcBase(NodeArcBase):
    'Base class for all graph arcs (or edges, or lines)'

    def isArc(self):
        return True

    def check_link(self, other):
        if other.isArc():
            raise GraphConnectionError('Connecting %r to %r!' % (self, other))

############################################################################

class FindRootsLeavesAll(object):
    'Collect all nodes in a graph, identifying roots and leaves'

    def __init__(self, node=None):
        self.roots = set()
        self.leaves = set()
        self.all = set()
        if node is not None:
            self.walk(node)

    def walk(self, node):
        if node not in self.all:
            self.all.add(node)
            did = False
            for item in node.inputs():
                did = True
                self.walk(item)
            if not did:
                self.leaves.add(item)
            did = False
            for item in node.outputs():
                did = True
                self.walk(item)
            if not did:
                self.roots.add(item)

############################################################################
