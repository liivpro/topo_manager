# Module objects

import graphbase

GraphError = graphbase.GraphConnectionError
safe_cmp = graphbase.safe_cmp

class ModuleBase(graphbase.NodeBase):
    'Base class for all modules'

    def __init__(self, modId=None, function=None, **kwargs):
        graphbase.NodeBase.__init__(self, **kwargs)
        self.modId = modId
        self.function = ModFunction(function)
        self.history = None

    def copy(self):
        'Make another module without connections or guids'
        new = self.__class__(name=self.name,
                             modId=self.modId,
                             descr=self.description,
                             function=self.function)
        new.history = self
        return new

    def isModule(self):
        return True

    def addConnector(self, item):
        if isinstance(item, (list, tuple)):
            for it in item:
                self.addConnector(it)
        else:
            if not item.isConnector():
                raise GraphError('Tried to add non-connector %s to module %s' \
                                 % (item, self))
            if item.isInput():
                self.addInput(item)
            else:
                self.addOutput(item)

    def __str__(self):
        out = [self.flavor.capitalize()]
        if self.name:
            out.append('"%s"' % (self.name,))
        return ' '.join(out)

    def description(self):
        if self.history is None:
            if self.descr is not None:
                return self.descr
            if self.flavor == 'inline' and self.function is not None:
                return '"%s": %s' % (self.name, self.function)
            return str(self)
        if self.name is None:
            s = '<None>'
        else:
            s = '"%s"' % (self.name,)
        return '%s from %s' % (s, self.history.description())

    
class ModuleInternal(ModuleBase):
     flavor = 'internal'


class ModuleExternal(ModuleBase):
     flavor = 'external'


class ModuleInline(ModuleBase):
     flavor = 'inline'

############################################################################

class ModFunction(object):

    def __init__(self, item=None):
        self.source = []
        self.code = None
        if isinstance(item, ModFunction):
            self.copy_in(item)
        elif item is not None:
            self.add(item)

    def __nonzero__(self):
        return len(self.source) > 0 or self.code is not None

    def __str__(self):
        return '; '.join([x.strip() for x in self.source])

    def copy_in(self, other):
        self.source = other.source[:]
        self.code = None

    def copy(self):
        return self.__class__(self)

    def add(self, it):
        #print 'add(%r)' % (it,)
        if isinstance(it, (list, tuple)):
            for x in it:
                self.add(x)
        elif isinstance(it, ModFunction):
            self.add(it.source)
        elif isinstance(it, (str, unicode)):
            it = it.rstrip()
            if it:
                self.source.append(str(it))
                self.code = None
        else:
            raise ValueError('%s.%s: unknown action: %r' % (
                self.__module__,
                self.__class__.__name__,
                it))

############################################################################
