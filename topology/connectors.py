# Connector objects

import graphbase

GraphError = graphbase.GraphConnectionError

class ConnectorBase(graphbase.NodeArcBase):

    def __init__(self, conId=None, ref=None, datatype=None, value=None,
                 **kwargs):
        graphbase.NodeArcBase.__init__(self, **kwargs)
        self.conId = conId
        self.ref = ref
        self.datatype = datatype
        self.value = value

    def copy(self):
        'Make another one, without guid or connections'
        return self.__class__(name=self.name,
                              descr=self.description,
                              conId = self.conId,
                              ref=self.ref,
                              datatype=self.datatype,
                              value=self.value)

    def isConnector(self):
        return True

    def isContent(self):
        return False

    def isControl(self):
        return False

    def isEvent(self):
        return False

    def isFeedback(self):
        return False

    def isInput(self):
        return False

    def isOutput(self):
        return False

    def check_link_in(self, other):
        if False and len(self.inputs()) > 0:
            self.debugDump(lambda: self.DumpIn(depth=4))
            raise GraphError('Connectors are 1-to-1 %s, %s' % (self, other))

    def check_link_out(self, other):
        if False and len(self.inputs()) > 0:
            self.debugDump(lambda: self.DumpOut(depth=4))
            raise GraphError('Connectors are 1-to-1 %s, %s' % (self, other))

    def __str__(self):
        out = []
        if self.isInput():
            out.append('Input')
        if self.isOutput():
            out.append('Output')
        out.append(self.flavor.capitalize())
        out.append('Connector')
        if self.name:
            out.append('"%s"' % (self.name,))
        return ' '.join(out)

############################################################################

class ConnInBase(ConnectorBase):

    def isInput(self):
        return True

    def isConnected(self):
        if self.inputs():
            return True
        return False

    def check_link_in(self, other):
        ConnectorBase.check_link_in(self, other)
        ## if not other.isChannel():
        ##     raise GraphError('Connecting %s to non-channel %s' % \
        ##                      (self, other))
        if self.flavor != other.flavor:
            raise GraphError('Connecting %s (%s) to wrong type %s (%s)' % \
                             (self, self.flavor, other, other.flavor))

    def check_link_out(self, other):
        ConnectorBase.check_link_out(self, other)
        ## if not other.isModule():
        ##     raise GraphError('Connecting %s to non-module %s' % \
        ##                      (self, other))

############################################################################

class ConnOutBase(ConnectorBase):

    def isOutput(self):
        return True

    def isConnected(self):
        if self.outputs():
            return True
        return False

    def check_link_in(self, other):
        ConnectorBase.check_link_in(self, other)
        ## if not other.isModule():
        ##     raise GraphError('Connecting %s to non-module %s' % \
        ##                      (self, other))

    def check_link_out(self, other):
        ConnectorBase.check_link_out(self, other)
        ## if not other.isChannel():
        ##     raise GraphError('Connecting %s to non-channel %s' % \
        ##                      (self, other))
        if self.flavor != other.flavor:
            raise GraphError('Connecting %s (%s) to wrong type %s (%s)' % \
                             (self, self.flavor, other, other.flavor))

############################################################################

class ConnInContent(ConnInBase):
    flavor = 'content'

    def isContent(self):
        return True

class ConnInControl(ConnInBase):
    flavor = 'control'

    def __init__(self, value=None, **kwarg):
        ConnInBase.__init__(self, **kwarg)
        self.value = value

    def isControl(self):
        return True

class ConnInEvent(ConnInBase):
    flavor = 'event'

    def isEvent(self):
        return True

class ConnInFeedback(ConnInBase):
    flavor = 'feedback'

    def isFeedback(self):
        return True

############################################################################

class ConnOutContent(ConnOutBase):
    flavor = 'content'

    def isContent(self):
        return True

class ConnOutControl(ConnOutBase):
    flavor = 'control'

    def __init__(self, value=None, **kwarg):
        ConnOutBase.__init__(self, **kwarg)
        self.value = value

    def isControl(self):
        return True

class ConnOutEvent(ConnOutBase):
    flavor = 'event'

    def isEvent(self):
        return True

class ConnOutFeedback(ConnOutBase):
    flavor = 'feedback'

    def isFeedback(self):
        return True
