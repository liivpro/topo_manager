# Manage argument parsing
#    Copyright (C) 2006-2009  by Sophisticated System Software, Inc.
#    Non-exclusive usage allowed as long as above copyright remains.
#
# The CmdOptions class handles command-line arguments and preference files.
# It generates usage messages and properly-formatted warning and fatal
# error messages.
#
# In this text we use the term "parameter" to refer to the description of
# the item for which we are looking, and "argument" for the actual item itself.
# In other words, the programmer defines the "parameters" and the user
# supplies the corresponding "arguments".
#
# There are 5 types of parameters:
#
# A. Positional parameters, usually referring to a file, that can take a
#    single value
# B. Positional parameters, usually referring to a list of files, that can
#    take zero or more values
# C. Flag parameters which have a short one-letter form "-x", a longer
#    form "--echs", or both.
# D. Flag parameters like above that take a value, "-x value", "--echs=value",
#    or "--echs value"
# E. Flag parameters like D, above, that can be repeated for multiple values
#
# Each parameter may be optional (the default) or required.
# Flag parameters with long flag names may enable a "no" flag version.
#
# Each possible parameter is constructed from the following values:
#
# 1. Optional short (one-letter) flag.  None for positional parameters.
#    (keyword: 'short')
# 2. Optional long flag.  None for positional parameters, but flag options
#    must have a short or long value.  (keyword: 'opt')
# 3. Optional argument name.  For flag options that do not take values
#    this must be None.  The name is used in descriptive text and value
#    lookup.  (keyword: 'arg')
# 4. Optional descriptive text.  This is used in usage messages.  If no
#    descriptive text for any of the parameters is supplied, only an
#    abbrieviated usage message is generated. (keyword: 'descr')
# 5. Optional flag string, built of the following characters in any order:
#      r   Required argument.  Raises an OptionError exception if missing.
#          Not written to pref files (see below).
#      m   Multiple values (i.e., types B and E, above).  Returns a list.
#      i   Convert to integer.  Raises an OptionError exception if not int.
#      f   Convert to float.  Raises an OptionError exception if not float.
#      l   Convert to long.  Raises an OptionError exception if not int.
#      c   Do NOT write to preference file (see below)
#      h   Hidden: Don't print in usage message
#      n   Only for flag parameters.  For parameter "<name>" makes it so
#          "no<name>" turns off the flag.  Mainly for overriding pref
#          files (see below).
#    (keyword: 'flags')
# 6. Optional default value.  (keyword: 'value')
#
#
# Set Up
# ======
# To create an CmdOptions object that recognizes an optional "-v" or "--verbose"
# flag, an optional "-o outfile" or "--outfile=outfile" argument, and at least
# one input file:
#
#  cmd = cmd_options.CmdOptions((
#                         ('v', 'verbose', None, 'Set for verbose reporting'),
#                         ('o', 'outfile', 'outfile', 'Output file'),
#                         (None, None, 'infile', 'Input file(s)', 'rm')))
#
# Alternatively, you can add to the list of recognized arguments using the
# AddOpt() method:
#
#  cmd = cmd_options.CmdOptions()
#  cmd.AddOpt('v', 'verbose', descr='Set for verbose reporting')
#  cmd.AddOpt('o', 'outfile, 'outfile', 'Output file')
#  cmd.AddOpt(arg='infile', descr='Input files(s)', flags='mr')
#
# Parsing The Arguments
# =====================
# Arguments are parsed by the ParseArgs() method.  It takes a list of arguments
# which defaults to sys.argv[1:], the standard command line arguments.  On
# error it will raise cmd_options.OptionError.
#
#  cmd = cmd_options.CmdOptions(...)
#    ...
#  try:
#      cmd.ParseArgs()
#  except cmd_options.OptionError, msg:
#      cmd.Usage(msg)
#
# This will parse the command line options and if there is an error, it will
# exit with the error and usage message.
#
# Getting The Argument Values
# ===========================
# The GetValue(key) method returns the value, if any for a 'key'.  The key
# is first looked up 'arg' value ('outfile' or 'infile' in the above example)
# and then by short or long flag value.
#
# If the parameter was never used, the default 'value' is returned, or None
# if a default wasn't supplied.  For flag parameters that do not take a value,
# an integer of the number of number of times the flag was specified is returned.
# For multi-valued argumemts (flags = "m"), a list of the string values (possibly
# one) is returned.  For the default single valued parameter a single string is
# returned.
#
# Note that all values are treated as strings.  It's up to the caller to convert
# to, e.g., integer.
#
# Special Case Processing of Final Required Positional Argument
# =============================================================
# Consider the case of the Unix "mv" and "cp" commands.  They take 1 or more
# source arguments and one final destination argument.  If you specified
#
#  cmd = cmd_options.CmdOptions((
#                    (None, None, 'infile', 'Input files', 'rm'),
#                    (None, None, 'output', 'Output directory', 'r'),
#                    ))
#
# The problem is that with special cases, the 'infile' parameter would gobble
# up all the positional parameters and nothing would end up in 'output'.
# As a special case (kludge) if the last position parameter is required
# and does not take multiple values, the last supplied positional value
# is set for this parameter and the rest of the parameters are processed
# as usual.
#
# Preference Files
# ================
# The paradigm we use here is to assume that anything that can be supplied
# on the command line can also be supplied in a preference file and vice-
# versa.  In other words, the command line and the preference file can
# contain the same parameters.  This is not enforced, we just make it easier
# to do it this way.
#
# The expected steps for using command line parsing and preference files
# are:
#
# 1) Parse the command line, but don't process yet
# 2) If there's no flag to skip the preference, then parse the preference
#    file, possibly using a path supplied on the command line
# 3) Process the arguments, with the command line arguments overriding the
#    preference file arguments
#
# A preference file consists of lines with the short or long option
# (without the leading "-" or "--") followed by a value, if the option
# takes a value.  Positional arguments can't be put in a preference file.
# Comments begin with a "#" which my be escaped "\#", and multiple option
# can be put on a line separated by ";", which may be escaped "\;".
#
# Example Preference File
# ==========================
# Of course the preference file contents is controlled by what you set
# for parameters in the CmdOptions class.  For this example, assume the
# following are legal:
#
#  quiet        # default to quiet mode
#  comment \#   # set comment character (for program, not pref file)
#  dog Fido The Great  # Note: no quotes. Same as '--dog="Fido The Great"
#                      #  on the command line
#  left KP4; up KP8; right KP6; down KP2  # multiple settings on one line
#  message Left\; no, Right  # Same as "--message="Left; no, Right" on cmd line
#
# Example Usage a Preference File
# ===============================
# Here's an example using a preference file:
#
#  cmd = cmd_options.CmdOptions((
#                 ('C', 'nopref', None, 'Skip preference file'),
#                 ('c', 'pref', 'pref-file', 'Preference file', None,
#                                               '~/.preference'),
#                  ...
#                 ))
#
#  pref = cmd.copy()       # make a copy of the setup for pref file
#  try:
#      cmd.ParseArgs()
#  except cmd_options.OptionError, msg:
#      cmd.Usage(msg)                      # print error and exit
#  if not cmd.GetValue('nopref'):          # if not told to skip
#      try:
#          pref.ReadPreferences(os.path.expanduser(cmd.GetValue('pref-file')))
#          cmd.SetDefaults(pref)           # make pref values defaults for
#                                          #  command line
#      except cmd_options.OptionError, msg:
#          cmd.PreferenceError(msg)            # print error and exit
#      except OSError:
#          pass                            # pref file not found
#  [Get the values from cmd...]
#
# Each CmdOption object can have at most one default object via SetDefaults(),
# by you can chain them together for arbitrarily by default.  For example,
# you could have a system-wide pref file that to put at the end of the
# local pref file:
#
#  sys_pref = cmd.copy()
#  try:
#      sys_pref.ReadPreferences(SystemPreferencePath)
#      pref.SetDefaults(sys_pref)
#  except OSError:
#      pass                                # no pref file
#
# Message Methods
# ===============
#    cmd.AddUsageLine(text)  Part of setup: Add verbatum lines to usage message.
#    cmd.AddUsageText(text)  Part of setup: Add line-wrapped lines to usage
#                            message.
#    cmd.UsageText()         Generator for lines in usage message.
#    cmd.Usage([message])    Output optional message followed by usage text
#                            on standard error, then exit
#    cmd.PreferenceHelpText()   Generator for lines in pref file help message.
#    cmd.PreferenceError([msg]) Output optional message followed by pref
#                            file help, then exit
#    cmd.ConfigHelpText()    Generator for lines in pref file help message.
#                            Override this to augment pref file help  text.
#    cmd.SetLineLen(n)       Set line length for usage message to <n>
#                            characters.  Defaults to 79.
#    cmd.SetGutter(n)        Sets the gutter width between columns in the usage
#                            message to <n>.  Defaults to 2.
#    cmd.Fatal(message)      Output program name and message on standard error
#                            and exit
#    cmd.Warning(message)    Output program name, "Warning",  and message on
#                            standard error and return
#    cmd.Message(message)    Output program name and message on standard error
#                            and return
#    cmd.PgmName()           Return program name (base part of sys.argv[0])
#
#
# Alternative Way Of Getting Values: Option Objects
# =================================================
# Internally, each possible parameter is represented by an option object.
# As the command line or a preference file gets parsed, these objects
# are filled with the supplied values.
#
# Methods For Getting Option Objects
# ==================================
#    cmd.get(key)      Get option by key, the same way cmd.GetValue() does,
#                      except it does not follow SetDefaults() links.
#    cmd.AddOpt(...)   In addition to adding the option to the list,
#                      this method returns the created option object.
#    cmd.Options()     Returns all the flag-style option objects.
#    cmd.Args()        Returns all the positional option objects.
#
# Using an Option Object
# ======================
#    opt.Value()       Returns the value (None, single value, list) the same
#                      way as cmd.GetValue() does.
#    opt.Short()       Short flag, if any.
#    opt.Opt()         Long flag, if any.
#    opt.Arg()         Argument name, if any.
#    opt.Description() Long-form description
#    opt.IsSet()       True if this parameter has been set.
#    opt.Default()     Return default option.  This is what gets returned for
#                      opt.Value() if opt.IsSet() is false.
#
# Writing A Preference File
# =====================
#    cmd.WritePreferences(file) Write all values that have been set
#                               (by ParseArg(), ReadPreferences(), or indirectly
#                               by SetDefaults()) to <file>.  This can be
#                               either a string of the file path or a file
#                               object open for writting.
# NOTE: If you have a parameter that says whether or not to save the arguments
# to a pref file, make sure you flag that parameter with a "c" so it doesn't
# get written to the pref file!  Otherwise, every time you run the program
# the value will be fetched from the pref file and the new settings will
# be written.  This is probably not what you want.

import sys
import getopt
import re
import exceptions
import time
import os
import os.path

PreferenceDirectory = '.preferences'

def PgmName():
    'Return program name'
    return os.path.splitext( os.path.split( sys.argv[0] )[1] )[0]


def PreferencePath(ext=None):
    '''
    Make a path to a program-specific preference file.
    If there is already a preference directory, put it there;
    Otherwise, put it as a dot file in home directory.
    '''
    file = PgmName()
    if ext:
        if not ext.startswith('.'):
            ext = '.' + ext
        file += ext
    dir = os.path.join(os.environ['HOME'], PreferenceDirectory)
    if not os.path.isdir(dir):
        dir = os.environ['HOME']
        file = '.' + file               # make it hidden
    return os.path.join(dir, file)

############################################################################
#
#    Exceptions

class Error(exceptions.Exception):
    def __str__(self):
        if isinstance(self.args, str):
            return self.args
        return ''.join([str(x) for x in self.args])


class OptionError(Error):
    def __init__(self, args=None):
        self.args = args

class OptionSetupError(Error):
    def __init__(self, args=None):
        self.args = args

class OptionAccessError(Error):
    def __init__(self, args=None):
        self.args = args

############################################################################

convTypeNames = {'i': 'integer', 'f': 'float', 'l': 'long'}

class val_base(object):
    conv_op = lambda y, x: x

    def __init__(self, dflt=None):
        self.dflt = dflt
        self.is_set = False
        self.value = None

    def copy(self):
        return self.__class__(self.dflt)

    def Default(self):
        return self.dflt

    def IsSet(self):
        return self.is_set

    def Set(self, x=None):
        self.is_set = True

    def Reset(self):
        pass

    def Value(self):
        if not self.is_set:
            return self.dflt
        return self.value

    def IsMultiple(self):
        return False

    def TakesValue(self):
        return False

    def conv(self, x):
        if x is None:
            return x
        if isinstance(x, list):
            return [self.conv(z) for z in x]
        return self.conv_op(x)

    def conv_int(self, x):
        try:
            y = int(x)
        except ValueError:
            raise OptionError, 'Expected integer, got "%s"' % (x)
        return y

    def conv_float(self, x):
        try:
            y = float(x)
        except ValueError:
            raise OptionError, 'Expected float, got "%s"' % (x)
        return y

    def conv_long(self, x):
        try:
            y = long(x)
        except ValueError:
            raise OptionError, 'Expected long, got "%s"' % (x)
        return y

    def SetConv(self, x):
        if x == 'i':
            self.conv_op = self.conv_int
        elif x == 'f':
            self.conv_op = self.conv_float
        elif x == 'l':
            self.conv_op = self.conv_long
        else:
            raise ValueError, 'Unrecognized conversion type "%s"' % (x)
        #
        # Convert default value, if any.  Note that all conversions
        # can be done multiple times, e.g.
        #    self.conv(x) == self.conv(self.conv(x))
        #
        if self.dflt is not None:
            try:
                y = self.conv(self.dflt)
            except OptionError, msg:
                raise OptionSetupError, 'Error in default: %s' % (msg)
            self.dflt = y

    def __str__(self):
        out = []
        if self.IsSet():
            out.append('Set')
            out.append(self.flavor)
            out.append('value=%r' % (self.value))
        else:
            out.append('Unset')
            out.append(self.flavor)
        if self.dflt is not None:
            out.append('default=%r' % (self.dflt))
        return ' '.join(out)

############################################################################

class val_flag(val_base):
    flavor = "flag"

    def __init__(self, dflt=None):
        val_base.__init__(self, dflt)
        self.value = dflt

    def Set(self, x=None):
        val_base.Set(self, x)
        if self.value is None:
            self.value = 0
        self.value += 1

    def Reset(self):
        '''Called by 'no'flag '''
        self.is_set = True
        self.value = 0


############################################################################

class val_single(val_base):
    flavor = "single"

    def Set(self, x):
        val_base.Set(self, x)
        self.value = self.conv(x)

    def TakesValue(self):
        return True

############################################################################

class val_multi(val_base):
    flavor = "multiple"

    def __init__(self, dflt=None):
        val_base.__init__(self, dflt)
        if dflt is None:
            dflt = []
        elif not isinstance(dflt, list):
            if isinstance(dflt, tuple):
                dflt = list(dflt)
            else:
                dflt = [dflt]
        self.dflt = dflt
        self.value = []

    def Set(self, x):
        if not self.is_set:
            self.is_set = True
            self.value = []
        self.value.append(self.conv(x))

    def Reset(self):
        self.is_set = True
        self.value = []

    def IsMultiple(self):
        return True

    def TakesValue(self):
        return True

############################################################################

class param_pos(object):
    """Describe a positional parameter"""
    patDash = re.compile(r'^-*')

    def __init__(self, descr, value, arg=None):
        self.descr = descr
        self.value = value
        self.arg = arg
        self.usage_cache = None
        self.is_pref = True
        self.pref_cache = None
        self.required = False
        self.hidden = False

    def copy(self):
        new = self.__class__(self.descr, self.value.copy(), self.arg)
        new.required = self.required
        new.hidden = self.hidden
        return new

    def Arg(self):
        return self.arg

    def IsOptional(self):
        return not self.required

    def IsMultiple(self):
        return self.value.IsMultiple()

    def TakesValue(self):
        return self.value.TakesValue()

    def IsOpt(self, a):
        return False

    def IsPreferenceFile(self):
        return self.is_pref

    def IsNoflag(self):
        return False

    def IsPositional(self):
        return True

    def IsHidden(self):
        return self.hidden

    def Description(self):
        if self.descr is None:
            return ''
        return self.descr

    def Name(self):
        return self.arg

    def __str__(self):
        if self.arg is not None:
            return '<%s>' % self.arg
        return '(argument)'

    def IsSet(self):
        return self.value.IsSet()

    def Default(self):
        return self.value.Default()

    def SetValue(self, x=None):
        self.value.Set(x)

    def Value(self):
        return self.value.Value()

    def UsageHelp(self):
        if self.usage_cache is None:
            out = ['', '', '', '']
            if self.arg:
                out[2] = '<' + self.arg + '>'
            x = self.Description()
            if x:
                out[3] = x
            self.usage_cache = out
        return self.usage_cache

    def PreferenceHelp(self):
        if self.pref_cache is None:
            name = self.Name()
            if not name:
                return None
            out = [name, '', '']
            if self.arg:
                out[1] = '<' + self.arg + '>'
            x = self.Description()
            if x:
                out[2] = x
            self.pref_cache = out
        return self.pref_cache

    def Dump(self):
        out = [str(self)]
        if not self.IsOptional():
            out.append('Required')
        if not self.IsPositional() and not self.IsPreferenceFile():
            out.append('NoPreference')
        if self.IsNoflag():
            out.append('"no" flag')
        if self.IsHidden():
            out.append('hidden')
        out.append('Value: %s' % (str(self.value)))
        yield ", ".join(out)

############################################################################

class param_flag(param_pos):
    """Describe a flag parameter"""

    def __init__(self, descr, value, arg=None, short=None, opt=None):
        param_pos.__init__(self, descr, value, arg)
        self.short = short
        self.opt = opt
        self.no_flag = False

    def __str__(self):
        if self.opt:
            if self.IsNoflag():
                s = '[no]' + self.opt
            else:
                s = self.opt
            if self.arg:
                return '--%s=<%s>' % (s, self.arg)
            return '--%s' % (s)
        if self.arg:
            return '-%s <%s>' % (self.short, self.arg)
        return '-%s' % (self.short)

    def copy(self):
        new = self.__class__(self.descr, self.value.copy(), self.arg,
                             self.short, self.opt)
        new.required = self.required
        new.is_pref = self.is_pref
        new.no_flag = self.no_flag
        return new

    def Short(self):
        return self.short

    def Opt(self):
        return self.opt

    def IsOpt(self, a):
        a = self.patDash.sub('', a)
        return (self.short is not None and a == self.short) or \
               (self.opt is not None and a == self.opt)

    def IsNoflag(self):
        return self.no_flag

    def IsPositional(self):
        return False

    def Name(self):
        if self.opt:
            return self.opt
        return self.short

    def UsageHelp(self):
        out = param_pos.UsageHelp(self)
        if self.short:
            out[0] = '-' + self.short
        if self.opt:
            out[1] = '--' + self.opt
        return out

    def NoOption(self):
        if self.no_flag:
            return param_noflag(self.value, "no" + self.opt, self.arg)
        return None

############################################################################

class param_noflag(param_flag):
    """Magically generated "no"<flag> object that resets the value"""

    def __init__(self, value, opt, arg=None):
        param_flag.__init__(self, None, value, arg=arg, opt=opt)
        self.no_flag = False

    def IsPreference(self):
        return False

    def TakesValue(self):
        return False

    def SetValue(self, x=None):
        self.value.Reset()

############################################################################


def parameter(short=None, opt=None, arg=None, descr=None,
             flags=None, dflt=None):
    global convTypeNames
    def blank2none(x):
        if x is not None and x == '':
            x = None
        return x
    if short is not None and opt is not None and len(short) > len(opt):
        x = short                   # swap
        short = opt
        opt = x
    short = blank2none(short)
    opt = blank2none(opt)
    arg = blank2none(arg)
    descr = blank2none(descr)
    if short and len(short) > 1:
        raise OptionSetupError, \
              'Short option "%s" must be one character' % (short)
    multiple = False
    required = False
    is_pref = True
    no_flag = False
    hidden = False
    d_out = []
    conv = None
    if flags:
        for f in flags:
            if f == 'm' or f == 'M':    # multiple
                multiple = True
            elif f == 's' or f == 'S':  # single (default)
                multiple = False
            elif f == 'r' or f == 'R':  # required
                required = True
            elif f == 'o' or f == 'O':  # optional (default)
                required = False
            elif f == 'c' or f == 'C':  # not for pref file
                is_pref = False
            elif f == 'h' or f == 'H':  # not in usage mesage
                hidden = True
            elif f == 'n' or f == 'N':  # "no<flag>" generation
                if not opt:
                    raise OptionSetupError, \
                          'Can\'t set "no"flag ("n") without a long ' + \
                          'version of flag "-%s"' % (short)
                no_flag = True
            elif convTypeNames.has_key(f.lower()):
                f = f.lower()
                if conv is not None and conv != f:
                    raise OptionSetupError, \
                          'Conflicting conversion types: "%s" and "%s"' % \
                          (conv, f)
                conv = f
            else:
                raise OptionSetupError, \
                      'Unrecognized option type flag "%s"' % (f)
    if conv:
        d_out.append(convTypeNames[conv])
    if multiple:
        d_out.append('can be repeated')
    if required:
        d_out.append('required')
        is_pref = False                   # no required option in pref
    if no_flag:
        d_out.append('"no%s" negates' % (opt))
    if dflt is not None:
        s = str(dflt)
        d_out.append('default: %s' % (s))
    if d_out:
        s = ' (' + ', '.join(d_out) + ')'
    else:
        s = ""
    if descr:
        descr += s
    else:
        descr = s
    descr = descr.strip()
    v = None
    if short or opt:
        if arg:
            if multiple:
                v = val_multi(dflt)     # multi-valued flag
            else:
                v = val_single(dflt)    # single-value flag
            opt = param_flag(descr, v, arg, short, opt)
        else:                           # plain old flag
            opt = param_flag(descr, val_flag(dflt), None, short, opt)
        opt.no_flag = no_flag           # allow "no<flag>"
        opt.is_pref = is_pref
    else:
        if multiple:
            v = val_multi(dflt)         # multi-valued positional
        else:
            v = val_single(dflt)        # single-value positional
        opt = param_pos(descr, v, arg)
    if conv and v is not None:
        v.SetConv(conv)
    opt.required = required
    opt.hidden = hidden
    return opt


############################################################################

class CmdOptions(object):
    patWs = re.compile(r'\s+')
    linelen = 78
    gutter = 2

    def __init__(self, ctrl=None):
        self.pgm_name = None
        self.default_options = None
        self.flags = []
        self.positionals = []
        self.more_usage = []
        if ctrl:
            for x in ctrl:
                a = tuple(x)
                self.AddOpt(*a)

    def copy(self):
        new = CmdOptions()
        new.pgm_name = self.pgm_name
        new.flags = [x.copy() for x in self.flags]
        new.positionals = [x.copy() for x in self.positionals]
        return new

    def add(self, item):
        if item.IsPositional():
            self.positionals.append(item)
        else:
            self.flags.append(item)
        
    def AddOpt(self, *args, **argss):
        retval = parameter(*args, **argss)
        self.add(retval)
        return retval

    def PgmName(self):
        if self.pgm_name is None:
            self.pgm_name = re.sub('.*/', '', sys.argv[0])
        return self.pgm_name

    def get_opt(self, k, dflt=None):
        for x in self.flags:
            if x.IsOpt(k):
                return x
        #
        # No match, look for "no" option
        #
        if len(k) > 2 and k.startswith('no'):
            k = k[2:]
            for x in self.flags:
                if not x.IsOpt(k):
                    continue
                z = x.NoOption()
                if z is not None:
                    return z
                break
        return dflt

    def Args(self):
        return self.positionals

    def Options(self):
        return self.flags

    def get(self, key, dflt=None):
        for opt in self.positionals + self.flags:
            if opt.arg is not None and opt.arg == key:
                return opt
        return self.get_opt(key, dflt)

    def SetDefaults(self, x):
        self.default_options = x

    def GetValue(self, key):
        """
        Find the value of a parameter.  This is where the default
        parameters (e.g., pref file defaults) get handled
        """
        opt = self.get(key)
        default_options = self.default_options
        if opt is not None:
            if not opt.IsSet() and default_options is not None:
                #
                # Defaulting to default options (e,g,, pref file)
                #
                return default_options.GetValue(key)
            return opt.Value()         # return the best we can
        if default_options is not None:
            #
            # We don't know the option but maybe the default guy does
            #
            return default_options.GetValue(key)
        raise KeyError, 'No option named "%s"' % (key)

    Opt = GetValue
    __getitem__ = GetValue

    def ParseArgs(self, args=None):
        pat = re.compile(r'(.*?)[:=](.*)')
        if args is None:
            args = sys.argv[1:]
        ap = 0
        plain = []
        while ap < len(args):
            p = args[ap]
            ap += 1
            if p == '--':
                break;
            if p.startswith('--'):
                key = p[2:]
                val = None
                m = pat.match(key)
                if m:
                    key = m.group(1)
                    val = m.group(2)
                opt = self.get_opt(key)
                if opt is None:
                    raise OptionError('Unrecognized option "%s"' % (p))
                if not opt.TakesValue():
                    if val:
                        raise OptionError, \
                              'Option "--%s" does not take a value' \
                              % (key)
                    opt.SetValue(1)
                else:
                    if not val:
                        if ap >= len(args):
                            raise OptionError, \
                                  'Missing value for option "--%s"' \
                              % (key)
                        val = args[ap]
                        ap += 1
                    opt.SetValue(val)
            elif p != '-' and p.startswith('-'):
                for key in p[1:]:
                    opt = self.get_opt(key)
                    if opt is None:
                        raise OptionError('Unrecognized option "-%s"' % (key))
                    if not opt.TakesValue():
                        val = True
                    else:
                        if ap >= len(args):
                            raise OptionError, \
                                  'Missing value for option "-%s"' \
                                  % (key)
                        val = args[ap]
                        ap += 1
                    opt.SetValue(val)
            else:
                plain.append(p)
        #
        # Plain (e.g., file) arguments
        #
        plain.extend(args[ap:])
        retval = len(plain)
        param_lim = len(self.positionals)
        if retval and param_lim and not self.positionals[-1].IsOptional() and \
               not self.positionals[-1].IsMultiple():
            #
            # Deal with required final argument
            #
            #print "Set %s to %s" % (str(self.positionals[-1]), plain[-1])
            self.positionals[-1].SetValue(plain.pop())
            param_lim -= 1
            #for p in self.positionals[param_lim].Dump(): print p
        param_cnt = 0
        for p in plain:
            if param_cnt >= param_lim:
                raise OptionError, \
                      'Too many arguments "%s"' % (p)
            opt = self.positionals[param_cnt]
            opt.SetValue(p)
            if not opt.IsMultiple():
                param_cnt += 1
        #
        # Scan for missing required arguments
        #
        for opt in self.flags + self.positionals:
            if not opt.IsOptional() and not opt.IsSet():
                raise OptionError, 'Missing required argument %s' % (str(opt))
        return retval

    def ReadPreferences(self, fd=None):
        if fd is None:
            fd = self.GetDefaultPrefPath('r')
            if fd is None:
                return False
        if isinstance(fd, str):         # path name
            try:
                infd = open(fd)
            except IOError:
                return False
            retval = self.ReadPreferences(infd)
            infd.close()
            return retval
        pat = re.compile(r'(\S*?)[:=\s]+(.*)')
        for line in fd:
            #
            # Remove comments, but allow "\#" to become non-comment "#"
            #
            parts = line.split('#')
            line = parts.pop(0)
            while len(parts) > 0 and len(line) > 1 and line[-1] == '\\':
                line = line[:-1] + '#' + parts.pop(0)
            #
            # Split at ";", but allow "\;" to become non-splitting ";"
            #
            parts = line.split(';')
            cmd = [parts.pop(0)]
            for p in parts:
                if len(cmd[-1]) and cmd[-1][-1] == '\\':
                    cmd[-1] = cmd[-1][:-1] + ';' + p
                else:
                    cmd.append(p)
            #
            # Look for:
            #   <key> [<value>]
            #
            for tok in cmd:
                key = tok.strip()
                if not key:             # empty
                    continue
                val = None
                m = pat.match(key)
                if m:
                    key = m.group(1)
                    val = m.group(2)
                #opt = self.get_opt(key)
                opt = self.get(key)
                if opt is None:
                    raise OptionError, 'Unrecognized option "%s"' % (key)
                if not opt.TakesValue():
                    if val:
                        raise OptionError, \
                              'Option "%s" does not take a value' % (key)
                    val = True             # counter for non-value flag
                else:
                    if not val:
                        raise OptionError, \
                              'Missing value for option "%s"' % (key)
                opt.SetValue(val)
        return True

    def WritePreferences(self, file=None):
        """
        Write a preference file based on current arguments and all
        the SetDefault() options.  Parameters that are not set are
        not written.
        """
        if file is None:
            file = self.GetDefaultPrefPath('w')
            if file is None:
                return False
        if isinstance(file, str):
            fd = open(file, 'w')
            retval = self.WritePreferences(fd)
            fd.close()
            return retval
        print >>file, time.strftime('# Preference file generated %Y/%m/%d %H:%M:%S')
        print >>file, ""
        for opt in self.positionals + self.flags:
            #print 'opt=%s' % (opt,)
            if not opt.IsPreferenceFile(): # not a candidate for pref file
                continue
            name = opt.Name()
            if not name:                # shouldn't happen
                print 'No Name'
                continue
            #
            # Walk back thru SetDefault() defaults until we find one
            # that is set.
            #
            cur = self
            xopt = opt
            while not xopt.IsSet():
                xopt = None             # not set, don't use it
                cur = cur.default_options
                if cur is None:
                    break
                xopt = cur.get_opt(name)
                if xopt is None:
                    break
            if xopt is None:
                xopt = opt              # restore
            if not xopt.IsSet():
                continue                # don't write defaults
            if not xopt.TakesValue():
                if xopt.IsNoflag() and not xopt.Value():
                    name = 'no' + name
                print >>file, name
            else:
                val = xopt.Value()
                if not xopt.IsMultiple():
                    val = [val]
                for v in val:
                    print >>file, name, re.sub('#', r'\#', re.sub(';', r'\;', v))
        print >>file, ""
        print >>file, '# End of pref file'

    def GetDefaultPrefPath(self, mode=''):
        '''
        Similar to PreferencePath(), except makes preference directory
        if it does not already exist.
        '''
        global PreferenceDirectory
        dir = os.path.join(os.environ['HOME'], PreferenceDirectory)
        if not os.path.isdir(dir):      # no preference directory
            if mode == 'r':             # reading
                return None             # return no joy
            if mode == 'w':             # writing
                os.makedirs(dir)        # create directory
            #
            # Otherise just interested in path
            #
        return os.path.join(dir, self.PgmName())

    def Fatal(self, msg):
        print >>sys.stderr, '%s: %s' % (self.PgmName(), str(msg))
        sys.exit(1)

    def Warning(self, msg):
        print >>sys.stderr, '%s: Warning: %s' % (self.PgmName(), str(msg))

    def Message(self, msg):
        print >>sys.stderr, '%s: %s' % (self.PgmName(), str(msg))

    def Usage(self, msg=None):
        if msg:
            print >>sys.stderr, '%s: %s' % (self.PgmName(), str(msg))
        for p in self.UsageText():
            print >>sys.stderr, p
        sys.exit(1)

    def PreferenceError(self, msg=None):
        if msg:
            print >>sys.stderr, '%s: %s' % (self.PgmName(), str(msg))
        for p in self.PreferenceHelpText():
            print >>sys.stderr, p
        sys.exit(1)

    def SetLineLen(self, x):
        self.linelen = x

    def SetGutter(self, x):
        self.gutter = x

    def UsageText(self):
        for p in self.UsageLine():
            yield p
        for p in self.UsageSummary():
            yield p
        for p in self.more_usage:
            yield p

    def UsageLine(self):
        #
        # Construct "Usage:" line(s)
        #
        words = [self.PgmName()]
        for opt in self.flags + self.positionals:
            if opt.IsHidden():
                continue
            s = str(opt)
            if opt.IsMultiple():
                s += ' ...'
            if opt.IsOptional():
                s = '[' + s + ']'
            words.append(s)
        s = 'Usage: '
        sb = ' ' * (len(s) + 2)
        for line in wrapwords(words, self.linelen - len(s) - 2):
            yield s + line
            s = sb

    def UsageSummary(self):
        need_where = True
        #
        # Construct description text
        #
        z = filter(lambda x: not x.IsHidden(), self.flags)
        for p in self.fmt_cols([x.UsageHelp() for x in z]):
            if need_where:
                yield "Where:"
                need_where = False
            yield p
        yield ""
        z = filter(lambda x: not x.IsHidden(), self.positionals)
        for p in self.fmt_cols([x.UsageHelp() for x in z]):
            if need_where:
                yield "Where:"
                need_where = False
            yield p

    def AddUsageLine(self, x=''):
        'Add usage lines printed verbatum'
        if isinstance(x, (list, tuple)):
            self.more_usage.extend(x)
        else:
            self.more_usage.append(str(x))

    def AddUsageText(self, text):
        'Add line wrapped usage text'
        if isinstance(text, (list, tuple)):
            text = ' '.join(text)
        words = re.split(r'\s+', text)
        self.more_usage.extend(wrapwords(words, self.linelen - 2))
    
    def PreferenceHelpText(self):
        yield "Preference file format:"
        cfg = filter(lambda x: x.IsPreferenceFile(), self.flags)
        for p in self.fmt_cols([x.PreferenceHelp() for x in cfg], center=0):
            yield p

    def fmt_cols(self, array, center=1):
        widths = []
        for v in array:
            w = [len(x) for x in v]
            n = len(w) - len(widths)
            if n > 0:
                widths += [0] * n
            for n in xrange(len(w)):
                if widths[n] < w[n]:
                    widths[n] =  w[n]
        if isinstance(self.gutter, int):
            gutter = ' ' * self.gutter
        else:
            gutter = self.gutter
        ng = len(gutter)
        if widths and widths[-1]:       # if any descriptions
            widths.pop()                # descr will wrap
            for n in xrange(len(widths)):
                if widths[n]:           # anything there?
                    widths[n] += ng     # add gutter
            for v in array:
                s = ' '
                if center:
                    for n in xrange(len(widths)):
                        s += v[n].center(widths[n])
                else:
                    for n in xrange(len(widths)):
                        s += v[n].ljust(widths[n])
                sb = ' ' * len(s)
                p = ''
                words = self.patWs.split(v[-1]) # words in description
                for line in wrapwords(words, self.linelen - len(s) - 2):
                    yield s + gutter + p + line
                    s = sb
                    p = '  '

    def EasyArgs(self):
        'Simplified interface to parse args and print usage'
        try:
            self.ParseArgs()
        except OptionError, msg:
            self.Usage(msg)

    EzArgs = EasyArgs

    def Dump(self):
        yield "PgmName = %s" % (self.PgmName())
        yield "Flags:"
        for f in self.Options():
            for p in f.Dump():
                yield "  " + p
        yield "Positionals:"
        for f in self.Args():
            for p in f.Dump():
                yield "  " + p
        if self.default_options is not None:
            yield "Default options:"
            for p in self.default_options.Dump():
                yield "  " + p

############################################################################    

def wrapwords(words, width=78):
    """Wrap words into nice text"""
    out = []
    curlen = width
    for w in words:
        n = len(w)
        if (curlen + n + 1) > width:
            out.append(w)               # put word on next line
            curlen = n
        else:
            out[-1] += " " + w
            curlen += n + 1
    return out

############################################################################

if __name__ == '__main__':
    #
    # Set up params
    #
    a = CmdOptions([
        ['g', 'grunt', 'sound', 'Grunting sound', None, "D'Oh!"],
        ['t', 'toy', 'toy', 'What to look for under the tree', 'm'],
        ])
    a.AddOpt(short='v', descr='Verbose')
    a.AddOpt(short='q', opt='quiet', descr='Quiet')
    a.AddOpt(short='f', opt='flag', flags='mn', \
             descr='This is fairly long description that' + \
             ' I hope will wrap lines.')
    a.AddOpt(short='o', opt='output', arg='outfile', descr='Output file')
    a.AddOpt(opt='doggie', arg='name', \
             descr='Argument that has only a long form')
    a.AddOpt('a', 'all', None, 'An "all" flag for when you want it all.')
    a.AddOpt(arg='infiles', flags='m', descr='Input files')
    a.AddOpt(arg='lastfile', flags='r', descr='Last required file')
    a.AddUsageLine('One little usage line')
    a.AddUsageLine(('Two short', 'usage lines.'))
    a.AddUsageText('Here is a rambling usage text that goes on and on ' + \
                   'and should probably go on several lines, wrapped nicely ' + \
                   'at appropriate word boundries.')
    a.AddUsageLine()
    a.AddUsageText(('Here is another rambling usage text that goes on and on',
                    'but this one is different, collected input,',
                    'still wrapped nicely ',
                    'at appropriate word boundries.'))
    #a.AddOpt(arg='files', flags='rm', descr='Required files')
    #
    # Get args
    #
    try:
        a.ParseArgs()
    except OptionError, s:
        a.Usage(s)
    #
    # Dump
    #
    for p in a.Dump():
        print p
    print '-' * 79
    #
    # Report
    #
    for opt in a.Options():
        print ' %s %r' % (str(opt), opt.Value())
    for opt in a.Args():
        print '    %r' % (opt.Value())
    a.WritePreferences(sys.stdout)
    a.PreferenceError("Example of preference error message")
